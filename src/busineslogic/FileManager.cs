﻿using SharedLibrary;
using starterpack.src.model;
using Vintagestory.API.Server;

namespace starterpack.src.busineslogic
{
    class FileManager
    {
        private ICoreServerAPI serverAPI;
        private static FileManager instance;

        public FileManager(ICoreServerAPI serverAPI)
        {
            this.serverAPI = serverAPI;
        }

        public static FileManager Create(ICoreServerAPI serverAPI)
        {
            if (instance == null)
            {
                instance = new FileManager(serverAPI);
            }
            else
            {
                instance.UpdateServerApi(serverAPI);
            }
            return instance;
        }

        public static void Dispose()
        {
            instance = null;
        }

        private void UpdateServerApi(ICoreServerAPI serverAPI)
        {
            this.serverAPI = serverAPI;
        }

        public static FileManager GetInstance()
        {
            return instance;
        }

        public void Init()
        {   
            LoadReceivedPlayers();   
        }

        public DefinedItemPackage LoadDefinedItemPackage(string filename)
        {
            return this.serverAPI.LoadOrCreateModDataFile<DefinedItemPackage>(filename, StarterPackMod.MOD_ID);
        }

        public void SaveDefinedItemPackage(string filename, DefinedItemPackage definedItemPackage)
        {
            this.serverAPI.SaveModDataFile(filename, definedItemPackage, StarterPackMod.MOD_ID);
        }

        public PlayersReceivedPackage LoadReceivedPlayers()
        {
            return this.serverAPI.LoadOrCreateModDataFile<PlayersReceivedPackage>(PlayersReceivedPackage.FILE_NAME, StarterPackMod.MOD_ID, true);
        }

        public void SaveReceivedPlayers(PlayersReceivedPackage definedItemPackage)
        {
            this.serverAPI.SaveModDataFile(PlayersReceivedPackage.FILE_NAME, definedItemPackage, StarterPackMod.MOD_ID, true);
        }
    }
}
