﻿using starterpack.src.model;
using System;
using System.Collections.Generic;
using Vintagestory.API.Common;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace starterpack.src.busineslogic
{
    class ItemManager
    {

        private static ItemManager instance;
        private ICoreServerAPI serverAPI;

        private Dictionary<string, model.DefinedItemPackage> definedItemPackagesMap = new Dictionary<string, DefinedItemPackage>();

        public ItemManager(ICoreServerAPI serverAPI)
        {
            this.serverAPI = serverAPI;
        }

        public static ItemManager Create(ICoreServerAPI serverAPI)
        {
            if (instance == null)
            {
                instance = new ItemManager(serverAPI);
            }
            else
            {
                instance.UpdateServerApi(serverAPI);
            }
            return instance;
        }
        private void UpdateServerApi(ICoreServerAPI serverAPI)
        {
            this.serverAPI = serverAPI;
        }

        public static ItemManager GetInstance()
        {
            return instance;
        }

        public static void Dispose()
        {
            instance = null;
        }

        public void CreateDefaultPackageForPlayers(List<CharacterClass> characterClasses)
        {
            characterClasses.ForEach(cc => {
                string filename = String.Format(DefinedItemPackage.FILE_NAME, cc.Code);
                definedItemPackagesMap.Add(cc.Code, FileManager.GetInstance().LoadDefinedItemPackage(filename));
                if (definedItemPackagesMap[cc.Code].DeliverItems.Count == 0) 
                {
                    definedItemPackagesMap[cc.Code].DeliverItems.Add(CreateItem("block", "torch-basic-lit-up", 3));
                    definedItemPackagesMap[cc.Code].DeliverItems.Add(CreateItem("item", "stick", 10));
                    definedItemPackagesMap[cc.Code].DeliverItems.Add(CreateItem("item", "flint", 10));
                    definedItemPackagesMap[cc.Code].DeliverItems.Add(CreateItem("item", "fruit-redcurrant", 20));
                    definedItemPackagesMap[cc.Code].DeliverItems.Add(CreateItem("item", "bread-spelt-perfect", 3));
                    FileManager.GetInstance().SaveDefinedItemPackage(filename, definedItemPackagesMap[cc.Code]);
                }
            });
        }

        private DeliverItem CreateItem(string type, string code, int amount)
        {
            return new DeliverItem
            {
                Type = type,
                Code = code,
                Amount = amount
            };
        }

        internal void DeliverItemsForPlayer(IServerPlayer player, string classCode)
        {
            definedItemPackagesMap[classCode].DeliverItems.ForEach(item2Deliver =>
            {
                try
                {
                    ItemStack itemStack;
                    if ("block".Equals(item2Deliver.Type.ToLower()))
                    {
                        itemStack = new ItemStack(serverAPI.World.GetBlock(new AssetLocation(item2Deliver.Code)), item2Deliver.Amount);
                        if (!IsItemStackBackpack(itemStack))
                        {
                            GiveItemStack(itemStack, player, item2Deliver);
                        }
                    }
                    else if ("item".Equals(item2Deliver.Type.ToLower()))
                    {
                        Item item = serverAPI.World.GetItem(new AssetLocation(item2Deliver.Code));
                        itemStack = new ItemStack(serverAPI.World.GetItem(new AssetLocation(item2Deliver.Code)), item2Deliver.Amount);
                        if (!IsItemBackpack(item) && !IsItemStackBackpack(itemStack))
                        {
                            GiveItemStack(itemStack, player, item2Deliver);
                        }
                        else
                        {
                            serverAPI.Logger.Warning("Object '{0}' is blacklisted, causing errors during API!", item2Deliver.Code.ToLower());
                        }
                    }
                    else
                    {
                        serverAPI.Logger.Warning("Type '{0}' not supported!", item2Deliver.Type.ToLower());
                        return;
                    }
                }
                catch (Exception e)
                {
                    serverAPI.Logger.Error("Adding '{0}-{1}' caused an exception, item or block not existing in game!", item2Deliver.Type.ToLower(), item2Deliver.Code.ToLower());
                    serverAPI.Logger.Error(e.Message);
                }
            });
            
        }

        private bool IsItemStackBackpack(ItemStack itemStack)
        {
            return Item.IsBackPack(itemStack);
        }

        private bool IsItemBackpack(Item item)
        {
            return item.StorageFlags == EnumItemStorageFlags.Backpack;
        }

        private void GiveItemStack(ItemStack itemStack, IServerPlayer player, DeliverItem item2Deliver)
        {
            if (player.InventoryManager.TryGiveItemstack(itemStack))
            {
                serverAPI.Logger.Event("{0}x '{2}-{1}' delivered", item2Deliver.Amount, item2Deliver.Code, item2Deliver.Type.ToLower());
            }
            else
            {
                serverAPI.Logger.Error("Deliver {0}x '{2}-{1}' failed", item2Deliver.Amount, item2Deliver.Code, item2Deliver.Type.ToLower());
            }
        }

        /*
        private void TestSnippet()
        {
            
                if (item2Deliver.IsInventory)
                {
                    IInventory backPackInv = player.InventoryManager.GetOwnInventory(GlobalConstants.backpackInvClassName);
                    foreach (ItemSlot bag in backPackInv.Where(b => b.Empty))
                    {
                        bag.Itemstack = new ItemStack(serverAPI.World.GetItem(new AssetLocation(item2Deliver.Code)));
                        bag.MarkDirty();
                        serverAPI.Logger.Event("'{1}-{0}' added", item2Deliver.Code, item2Deliver.Type.ToLower());
                        break;
                    }
                }
                else
                {
                
        }
        */
    }
}
