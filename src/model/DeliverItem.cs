﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starterpack.src.model
{
    class DeliverItem
    {
        public string Type { get; set; }
        public string Code { get; set; }
        public int Amount { get; set; }
    }
}
