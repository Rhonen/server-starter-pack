﻿using System;
using System.Collections.Generic;

namespace starterpack.src.model
{
    class PlayersReceivedPackage
    {
        public static String FILE_NAME = "starterpack.playersreceivedpackage.json";
        public List<string> playerUuidList { get; set; } = new List<string>();
    }
}
