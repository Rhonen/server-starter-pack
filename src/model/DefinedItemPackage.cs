﻿using System.Collections.Generic;

namespace starterpack.src.model
{
    class DefinedItemPackage
    {
        public static string FILE_NAME = "starterpack.deliveritempackage.{0}.json";

        public List<DeliverItem> DeliverItems { get; set; } = new List<DeliverItem>();
    }
}
