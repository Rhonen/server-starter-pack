﻿using starterpack.src.busineslogic;
using starterpack.src.model;
using System.Collections.Generic;
using Vintagestory.API.Common;
using Vintagestory.API.Datastructures;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace starterpack.src
{
    class StarterPackMod : ModSystem
    {
        public static readonly string MOD_ID = "starterpackmod";
        private FileManager fileManager;
        private ItemManager itemManager;

        private PlayersReceivedPackage playersReceivedPackage;
        private ICoreServerAPI sapi;

        private List<CharacterClass> characterClasses = new List<CharacterClass>();

        public List<TreeModifiedListener> OnWatchedAttributesClassChanged { get; private set; }

        public override void StartServerSide(ICoreServerAPI api)
        {
            base.StartServerSide(api);
            this.sapi = api;
            
            fileManager = FileManager.Create(sapi);
            itemManager = ItemManager.Create(sapi);

            api.Event.PlayerNowPlaying += PlayerNowPlaying;
            api.Event.SaveGameLoaded += StartMod; 
            sapi.Event.ServerRunPhase(EnumServerRunPhase.Shutdown, OnShutDown);
            //Patch300(api);
        }

        private void OnShutDown()
        {
            ItemManager.Dispose();
            FileManager.Dispose();
        }

        private void StartMod()
        {
            characterClasses = sapi.Assets.Get("config/characterclasses.json").ToObject<List<CharacterClass>>();
            sapi.World.Logger.VerboseDebug("Loaded characterclasses: {0}", characterClasses.Count);

            fileManager.Init();
            itemManager.CreateDefaultPackageForPlayers(characterClasses);
            playersReceivedPackage = fileManager.LoadReceivedPlayers();

            sapi.World.Logger.Event("started 'Starter-Pack-Mod' successfull");
        }

        private void PlayerNowPlaying(IServerPlayer player)
        {
            //when player already received data, abort
            if (playersReceivedPackage.playerUuidList.Contains(player.PlayerUID))
            {
                sapi.World.Logger.Event("Player {0} already received startpackage before, skipped", player.PlayerName);
                return;
            }

            sapi.World.Logger.Event("Player {0} registered for RegisterModifiedListener ", player.PlayerName);
            player.Entity.WatchedAttributes.RegisterModifiedListener("characterClass", () => ActionModifiedListener(player) );
            
        }

        private void ActionModifiedListener(IServerPlayer player)
        {
            sapi.World.Logger.VerboseDebug("Player {0} WatchedAttributes changed", player.PlayerName);
            string classCode = player.Entity.WatchedAttributes.GetString("characterClass");
            if (classCode != null)
            {
                sapi.World.Logger.Event("Player {0} selected class {1} , deliver items", player.PlayerName, classCode);
                ItemManager.GetInstance().DeliverItemsForPlayer(player, classCode);
                playersReceivedPackage.playerUuidList.Add(player.PlayerUID);
                FileManager.GetInstance().SaveReceivedPlayers(playersReceivedPackage);
            }
        }
    }
}
